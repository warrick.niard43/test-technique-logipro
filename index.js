// FIZZ
function fizz() {

    const base = document.querySelector('#base');
    const fizz = document.querySelector('#fizz');
    const result = [];
    const resultBase = [];

    // incrémente i jusqu'à 100
    for (var i = 1; i < 101; i++) {
        
        // Si i est divisible par 3, Fizz, push i dans result[]
        if (i % 3 == 0) {
            console.log("Fizz :" + i);
            result.push(i);
        }
        
        resultBase.push(i);
    }
    
    // return result[] dans la div avec l'id "fizz"
    return (
        fizz.innerHTML = result,
        base.innerHTML = resultBase
    )
}

// BUZZ
function buzz() {
    
    const base = document.querySelector('#base');
    const buzz = document.querySelector('#buzz');
    const result = [];
    const resultBase = [];
    
    // incrémente i jusqu'à 100
    for (var i = 1; i < 101; i++) {
        
        // Si i est divisible par 5, Buzz, push i dans result[]
        if (i % 5 == 0) {
            console.log("Buzz :" + i);
            result.push(i);
        }

        resultBase.push(i);
    }

    // return result[] dans la div avec l'id "buzz"
    return (
        buzz.innerHTML = result,
        base.innerHTML = resultBase
    )
}

// FIZZBUZZ
function fizzbuzz() {

    const fizzbuzz = document.querySelector('#fizzbuzz');
    const base = document.querySelector('#base');
    const result = [];
    const resultBase = [];

    // incrémente i jusqu'à 100
    for (var i = 1; i < 101; i++) {

        // Si i est divisible par Fizz(3) et Buzz(5), Fizzbuzz, push i dans result[]
        if (i % 3 == 0) {
            if (i % 5 == 0){
                console.log("FizzBuzz :" + i);
                result.push(i);
            }
        }
        
        resultBase.push(i);
    }
    
    // return result[] dans la div avec l'id "fizzbuzz"
    return (
        fizzbuzz.innerHTML = result,
        base.innerHTML = resultBase
    )
}
