// FIZZ
function fizz(i) {

    // Si i est divisible par 3 (Fizz), return i
    if (i % 3 == 0) {
        return i
    }
}

// BUZZ
function buzz(i) {

    // Si i est divisible par 5 (Buzz), return i
    if (i % 5 == 0) {
        return i
    }
}

// FIZZBUZZ
function fizzbuzz() {

    const fizzbuzz = document.querySelector('#fizzbuzz');
    const result = [];

    // incrémente i jusqu'à 100
    for (var i = 1; i < 101; i++) {

        // Si i est divisible par Fizz et Buzz (% 3 & %5) push i dans result[]
        if (fizz(i) && buzz(i)) {
            console.log('FizzBuzz' + i);
            result.push('FizzBuzz : ' + i + "<br/>");
        }
        
        // Si i est divisible par Buzz (% 5) push i dans result[]
        else if (buzz(i)) {
            console.log('Buzz' + i);
            result.push('Buzz : ' + i + "<br/>");
        }

        // Si i est divisible par Fizz (% 3) push i dans result[]
        else if (fizz(i)) {
            console.log('Fizz' + i);
            result.push('Fizz : ' + i + "<br/>");
        }

        // Push i dans result[] dans tous les cas
        else result.push(i + "<br/>");
        
    }

    // return result[] dans la div avec l'id "fizzbuzz"
    return (
        fizzbuzz.innerHTML = result
    )
}