# Fizzbuzz

## installation

Cloner un depot en ligne à partir du dossier dans lequel on souhaite créer le dépôt local :

```sh
# A la racine du projet
git clone "url" .
```

### Exemple

```sh
mkdir helloworld
cd helloworld
git clone https://gitlab.com/warrick.niard43/test-technique-logipro.git .
```

## Utilisation

Pour VSCode, vous pouver faire un clic droit sur le fichier "index.html" et "Open with Live Server" pour ouvrir.

Sinon vous pouvez passer par les dossier, "Ouvrir avec >" et selectionner le navigateur que vous voulez.